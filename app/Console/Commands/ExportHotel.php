<?php
/**
 * Created by PhpStorm.
 * User: vova
 * Date: 19.01.18
 * Time: 1:07
 */

namespace App\Console\Commands;

use App\Hotel;
use Illuminate\Console\Command;

class ExportHotel extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'hotel:export';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send drip e-mails to a user';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $commonArray = [];
        $hotels = Hotel::all();

//        $hotels = $hotels->sortByDesc('country');
//
//        $groups = $hotels->groupBy('country');

        mkdir(storage_path('jsons'), 0777);

        foreach ($hotels->toArray() as $item ) {
            $commonArray[$item['country']][] = $item;
        }

        foreach ($commonArray as $key => $group) {

            $key = str_replace(['/', '\'', '.'], '', $key);

            try {
                echo file_put_contents(storage_path('jsons/'.$key.'.json'), json_encode($group, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE), FILE_APPEND);
            } catch (\Exception $exception) {
                var_dump($exception->getMessage());
                continue;
            }
        }
    }
}