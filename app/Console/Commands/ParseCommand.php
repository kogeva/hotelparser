<?php
/**
 * Created by PhpStorm.
 * User: vova
 * Date: 19.01.18
 * Time: 1:07
 */

namespace App\Console\Commands;

use DOMElement;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use function GuzzleHttp\Promise\all;
use GuzzleHttp\Psr7\Response;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ParseCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:homepage';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send drip e-mails to a user';

    private $httpClient;

    const HOTEL_HOST = 'https://joinup.ua';

    public function __construct()
    {
        parent::__construct();

        $this->httpClient = new Client();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $linksContent = file_get_contents(storage_path('links.txt'));

        $links = explode("\n", $linksContent);
        $promises = [];
        $linkChunk = array_chunk($links, 2);

        foreach ($linkChunk as $key => $chunk) {

            $iteration = $key * 2;

            echo ($iteration) . "\n";
//            if ($key == 10) {
//                break;
//            }

            foreach ($chunk as $link) {

//                dd($link);
//                var_dump($link);
                list($id, $path) = explode(';', $link);

//                if ($path !== '/hotel/now-jade-riviera-cancun-resort-amp-spa/') {
//                    continue;
//                } else {
//                    dd('ITS');
//                }
////
                if ($path == 'false' || $iteration <= 8716) {
//                if ($path == 'false') {
                    continue;
                }

                $promise = $this->httpClient->requestAsync('GET', self::HOTEL_HOST . $path);

                $promise->then(function (Response $response) use ($path) {
                    try {
                        $document = \phpQuery::newDocument((string)$response->getBody());

                        $countryAndResort = explode(',', $document->find('.showGoogleMap')->text());
                        $hotelName = $document->find('div.left_side h1')->text();
                        $stars = preg_replace('/\s/', '', $document->find(".hotel-rating.showHotelModal:eq(0)")->text());
                        $description = trim($document->find('div.hotel_short_description')->text());
                        $hotelMap = $document->find('.rekl_photo img')->attr('src');
                        $location = $document->find('div.hotel_desc_loc')->text();
                        $video = $document->find('.fotorama a')->attr('href');
                        preg_match_all('/\/hotel\/(.*)\//', $path, $hotelSlug);

                        $hotel = $hotelSlug[1][0];

                        $hotelSlug = 'hotels/' . $hotelSlug[1][0];


                        mkdir(storage_path($hotelSlug, 0777));
                        mkdir(storage_path($hotelSlug . '/images', 0777));
                        mkdir(storage_path($hotelSlug . '/rooms', 0777));

                        if (!empty($hotelMap)) {
                            mkdir(storage_path($hotelSlug . '/map', 0777));

                            $pathInfo = pathinfo($hotelMap);

                            $this->httpClient->requestAsync(
                                'GET',
                                self::HOTEL_HOST . $hotelMap,
                                ['sink' => storage_path($hotelSlug . '/map/' . $pathInfo['basename'])]
                            );

                            $hotelMap = $hotelSlug . '/map/' . $pathInfo['basename'];
                        }

                        $relaxationTypes = [];
                        $privileges = [];
                        $commonInfo = [];
                        $roomTypes = [];
                        $services = [];
                        $images = [];
                        $contacts = [];


                        $contactsNodes = $document->find('div.hotel_contacts div');
                        foreach ($contactsNodes->elements as $element) {

                            if (empty($element->textContent)) {
                                continue;
                            }

                            $parts = explode(':', $element->textContent);

                            if (count($parts) > 1) {
                                $contacts[$parts[0]] = $parts[1];
                            } else {
                                $contacts['site'] = $parts[0];
                            }

                        }


                        $imagesNodes = $document->find("img[data-popup-url]");

                        /** @var DOMElement $element */
                        foreach ($document->find('ul.icons.cf li')->elements as $element) {
                            $relaxationTypes[] = $element->getAttribute('data-title');
                        }

                        foreach ($imagesNodes->elements as $element) {

                            $value = $element->attributes['src']->value;
                            $pathInfo = pathinfo($value);

                            $images[] = $hotelSlug . '/images/' . $pathInfo['basename'];

                            $this->httpClient->requestAsync(
                                'GET',
                                self::HOTEL_HOST . $value,
                                ['sink' => storage_path($hotelSlug . '/images/' . $pathInfo['basename'])]
                            );

                        }

                        /** @var DOMElement $element */
                        foreach ($document->find('.hotel_advantage_box>ul>li')->elements as $element) {
                            $privileges[] = $element->textContent;
                        }

                        foreach ($document->find('table.general_info>tr>th')->elements as $key => $element) {
                            $commonInfo[] = [
                                'name' => $element->textContent,
                                'value' => $document->find("table.general_info>tr>td:eq($key)")->text()
                            ];
                        }

                        foreach ($document->find('div.hotel_room_types>div>ul>li>a')->elements as $idx => $element) {

                            $roomDescription = $document->find($element->attributes['id']->value . ' p:eq(1)')->text();

                            $specifications = [];
                            $options = [];
                            $roomImages = [];

                            $nodes = $document->find($element->attributes['id']->value . " table[class='hotel_room_table_num']>tr");

                            foreach ($nodes->elements as $key => $value) {

                                $roomImagesNodes = $nodes->eq($key)->find('img');

                                if ($roomImagesNodes->size() > 0) {
                                    mkdir(storage_path($hotelSlug . "/rooms/$idx", 0777));

                                    foreach ($roomImagesNodes->elements as $value) {
                                        $pathInfo = pathinfo($value->attributes['src']->value);

                                        $this->httpClient->requestAsync(
                                            'GET',
                                            self::HOTEL_HOST . $value->attributes['src']->value,
                                            ['sink' => storage_path($hotelSlug . "/rooms/$idx/" . $pathInfo['basename'])]
                                        );

                                        $roomImages[] = $hotelSlug . "/rooms/$idx/" . $pathInfo['basename'];
                                    }
                                }

                                if (count($roomImages) > 0) {
                                    if (!empty($nodes->eq($key)->find('td:eq(1)')->text())) {
                                        $specifications[$nodes->eq($key)->find('td:eq(1)')->text()] = $nodes->eq($key)->find('td:eq(2)')->text();
                                    }
                                } else {
                                    if (!empty($nodes->eq($key)->find('td:eq(0)')->text())) {
                                        $specifications[$nodes->eq($key)->find('td:eq(0)')->text()] = $nodes->eq($key)->find('td:eq(1)')->text();
                                    }
                                }
                            }

                            $nodes = $document->find($element->attributes['id']->value . "  ul.hotel_room_options_left>li");

                            foreach ($nodes->elements as $key => $value) {
                                $options[] = $value->textContent;
                            }

                            $roomTypes[] = [
                                'type' => $element->textContent,
                                'images' => $roomImages,
                                'description' => $roomDescription,
                                'specifications' => $specifications,
                                'options' => $options
                            ];
                        }

                        foreach ($document->find('div#hotel_infrastructure_services_tabs>ul>li>a')->elements as $element) {

                            $nodes = $document->find($element->attributes['id']->value . ' ul>li');
                            $options = [];

                            foreach ($nodes->elements as $value) {
                                $options[] = $value->textContent;
                            }

                            $services[] = [
                                'type' => $element->textContent,
                                'options' => $options
                            ];
                        }

                        $item = [
                            'hash' => md5($hotel),
                            'hotel_slug' => $hotel,
                            'country' => $countryAndResort[0],
                            'resort' => $countryAndResort[1],
                            'images' => $images,
                            'hotel_name' => $hotelName,
                            'stars' => $stars,
                            'contacts' => $contacts,
                            'description' => $description,
                            'hotel_map' => ['url' => $hotelMap],
                            'relaxation_types' => $relaxationTypes,
                            'privileges' => $privileges,
                            'common_info' => $commonInfo,
                            'location' => $location,
                            'rooms' => $roomTypes,
                            'infrastructures_services' => $services,
                            'video' => (!empty($video)) ? $video : false
                        ];

                        DB::collection('hotels_new')->where('hash', md5($hotel))
                            ->update($item, ['upsert' => true]);

                        $this->httpClient->requestAsync('GET', 'https://joinup.ua/content/uploads/2017/11/Standart-room-300x225.jpeg', [
                            'curl' => [
                                CURLOPT_HTTPHEADER => ['Expect:']
                            ],
                            'sink' => '/tmp/hello'
                        ]);

                    }
                    catch (RequestException $exception) {
                        echo $exception->getMessage()."\n";
                    }
                    catch
                    (\Throwable $exception) {
                        DB::collection('bad_hotels')->where('path', $path)
                            ->update(['path' => $path], ['upsert' => true]);
                        var_dump($exception->getMessage(), $path);
                    }
                });

                $promises[] = $promise;
            }
            try {
                all($promises)->wait();
            } catch (\Throwable $exception) {
                $promises = [];
                DB::collection('bad_hotels_two')->where('path', $path)
                    ->update(['path' => $path], ['upsert' => true]);
                echo $exception->getMessage()."\n";
                echo $exception->getTraceAsString()."\n";
                sleep(10) ;
                continue;
            }
        }
//        echo file_put_contents(storage_path('items.json'), json_encode($this->resultStorage, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE), FILE_APPEND);
    }
}