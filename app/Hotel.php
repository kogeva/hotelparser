<?php
/**
 * Created by PhpStorm.
 * User: vova
 * Date: 27.01.18
 * Time: 3:45
 */

namespace App;


use Jenssegers\Mongodb\Eloquent\Model;

class Hotel extends Model
{
    protected $table = 'hotels_new';
}